#! /usr/bin/env python3

from pylab import *
from scipy.linalg import hilbert, invhilbert

def test (n):
    H = hilbert(n)
    b = ones(n)
    return norm(solve(H,b) - dot(invhilbert(n),b)), cond(H)

test = vectorize(test)

nn = arange(1,16)
errors, conds = test(nn)

figure()
semilogy(nn, errors, '-',
         nn, conds, '*')
legend(('Error',
        'Condition Number'),
       loc = 'lower right')
title('Hilbert matrix test case for numpy.linalg.linalg.solve')
show()
