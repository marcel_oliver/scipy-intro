#! /usr/bin/env python3

from pylab import *
from mpl_toolkits.mplot3d import Axes3D

figure()
ax = subplot(111, projection='3d')

x = linspace(-3,3,40)
xx, yy = meshgrid(x,x)
zz = exp(-xx**2-yy**2)

ax.plot_surface(xx, yy, zz,
                rstride=1,
                cstride=1,
                cmap=cm.binary,
                edgecolor='k',
                linewidth=0.2)

ax.set_xlabel(r'$x$')
ax.set_ylabel('$y$')
ax.set_zlabel('$z$')
title(r'The function $z=\exp(-x^2-y^2)$')
savefig('plot3d.pdf')

show()

