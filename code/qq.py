#! /usr/bin/env python3

from pylab import *

p = poisson(lam=10, size=4000)
m = mean(p)
s = std(p)
n = normal(loc=m, scale=s, size=p.shape)
#
#### To use an explicitly available PPF of the normal distribution,
#### replace last line by:
#
# from scipy.stats import norm
# n = norm.ppf((0.5+arange(len(p)))/len(p), loc=m, scale=s)

a = m-4*s
b = m+4*s

figure()
plot(sort(n), sort(p), 'o', color='0.85',
     markeredgewidth=0.2, markeredgecolor='k',)
plot([a,b], [a,b], 'k-')
xlim(a,b)
ylim(a,b)
xlabel('Normal Distribution')
ylabel('Poisson Distribution with $\lambda=10$')
grid(True)
savefig('qq.pdf', bbox='tight')
show()
     
