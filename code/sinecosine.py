#! /usr/bin/env python3

from pylab import *

N = 100  # Number of plot points
xmin = 0
xmax = pi

xx = linspace(xmin, xmax, N)
yy = sin(xx)
zz = cos(xx)

rc('text', usetex=True) # Use TeX typesetting for all labels
figure(figsize=(5,3))
plot(xx, yy, 'k-',
     xx, zz, 'k:')
xlabel('$x$')
ylabel('$y$')
title('Sine and Cosine on the Interval $[0,\pi]$')
legend(('$y=\sin(x)$',
        '$y=\cos(x)$'),
       loc = 'lower left')
annotate(r'$\sin(\frac{\pi}{4})=\cos(\frac{\pi}{4})=\frac{1}{\sqrt{2}}$',
         xy = (pi/4+0.02, 1/sqrt(2)),
         xytext = (pi/4 + 0.22, 1/sqrt(2)-0.1),
         size = 11,
         arrowprops = dict(arrowstyle="->"))
xlim(xmin,xmax)

savefig('sinecosine.pdf', bbox='tight')
show()

