#! /usr/bin/env python3

from pylab import *

bdf6 = array([147, -360, 450, -400, 225, -72, 10])
R = roots(bdf6)

c = exp(1j*linspace(0,2*pi,200))

figure()
plot(R.real, R.imag, 'ko',
     c.real, c.imag, 'k-')
title('Roots of the first characteristic polynomial for BDF6')
savefig('zero-stability.pdf', bbox='tight')

# It is known that 1 is a root, so we devide it out because the
# root condition cannot be decided numerically in this marginal case.

z1 = array([1,-1])
reduced_bdf6 = polydiv(bdf6, z1)[0]

if max(abs(roots(reduced_bdf6)))>1.0:
    print("BDF6 is not zero-stable")
else:
    print("BDF6 is zero-stable")

# Now lets plot the region of absolute stability

rhs = array([60, 0, 0, 0, 0, 0, 0])

def stabroots(z):
    R = roots(bdf6 - z*rhs)
    return max(abs(R))
stabroots = vectorize(stabroots)

x,y = meshgrid(linspace(-10,30,350),
               linspace(-22,22,350))
z = stabroots(x + 1j*y)

figure()
contour(x, y, z, [1.0], colors='k')
contourf(x, y, z, [-1.0,1.0], colors=['0.85'])
title('Absolute stability region for BDF6')
grid(True)
savefig('abs-stability.pdf', bbox='tight')

show()
