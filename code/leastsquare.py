#! /usr/bin/env python

from pylab import *
from scipy.linalg import lstsq

def fitline (xx, yy):
    A = c_[xx, ones(xx.shape)]
    m, b = lstsq(A, yy)[0]
    return m, b

xx = arange(21)
yy = xx + normal(size=xx.shape)
m, b = fitline(xx, yy)

figure()
plot(xx, yy, '*',
     xx, m*xx+b, '-')
legend(('Data', 'Linear least square fit'),
       loc = 'upper left')
show()
     
